<!-- app/views/eloquent.blade.php -->

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Clients Table</title>

    <!-- CSS -->
    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <style>
        body { padding-top:50px; } /* add some padding to the top of our site */
        @media (min-width: 1200px){
            .container {
              max-width: 1794px;
          }
        }
        
    </style>
</head>
<body class="container">

  <div class="col-sm-12 col-sm-offset-2">
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModal">
      Add Client
    </button>

    <!-- Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="addModalLabel">Add Client</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          {!! Form::open([ 'method' => 'POST', 'class' => 'margin-t-5', 'route' => ['clients.store'], 'id' => 'createForm' ]) !!}
          <div class="modal-body">
              <div class="form-group row">
                <div class="col-sm-4">
                  {!! Form::text('first_name', null, ['class' => 'form-control', 'id' => 'first_name', 'placeholder' => 'First Name']) !!}
                </div>
                <div class="col-sm-4">
                  {!! Form::text('middle_name', null, ['class' => 'form-control', 'id' => 'middle_name', 'placeholder' => 'Middle Name']) !!}
                </div>
                <div class="col-sm-4">
                  {!! Form::text('last_name', null, ['class' => 'form-control', 'id' => 'last_name', 'placeholder' => 'Last Name']) !!}
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-4">
                  {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Email']) !!}
                </div>
                <div class="col-sm-4">
                  {!! Form::text('landline', null, ['class' => 'form-control', 'id' => 'landline', 'placeholder' => 'Landline']) !!}
                </div>
                <div class="col-sm-4">
                  {!! Form::text('cellphone', null, ['class' => 'form-control', 'id' => 'cellphone', 'placeholder' => 'Cellphone']) !!}
                </div>
              </div>

              <div class="form-group row">
                <label for="birthdate" class="col-sm-4 col-form-label" style="text-align:right;">Birth date</label>
                <div class="col-sm-4">
                  {!! Form::date('birthdate', null, ['class' => 'form-control', 'id' => 'birthdate']) !!}
                </div>
                <div class="col-sm-4">
                  <div class="form-check">
                    <label class="form-check-label">
                      <input class="form-check-input" type="radio" name="gender" id="male" value="male" checked>
                      Male
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input class="form-check-input" type="radio" name="gender" id="female" value="female">
                      Female
                    </label>
                  </div>
                </div>
              </div>

              <div class="form-group row">
                <label for="address" class="col-sm-4 col-form-label" style="text-align:right;">Address</label>
                <div class="col-sm-8">
                  {!! Form::text('address', null, ['class' => 'form-control', 'id' => 'address']) !!}
                </div>
              </div>

              <hr>

              <div class="form-group row">
                <label for="applied_loan_amount" class="col-sm-4 col-form-label" style="text-align:right;">Applied Loan Amount</label>
                <div class="col-sm-4">
                  {!! Form::text('applied_loan_amount', null, ['class' => 'form-control', 'id' => 'applied_loan_amount']) !!}
                </div>
                <div class="col-sm-4">
                  {!! Form::select('loan_type',  ['' => 'Loan Type', 'lite' => 'Lite Loan', 'salary' => 'Salary Loan', 'personal' => 'Personal Loan', 'business' => 'Business loan'], null, ['id' => 'loan_type', 'class' => 'form-control', 'required']) !!}
                </div>
              </div>

              <div class="form-group row">
                <label for="interest_rate" class="col-sm-4 col-form-label" style="text-align:right;">Interest Rate</label>
                <div class="col-sm-4">
                  {!! Form::text('interest_rate', null, ['class' => 'form-control', 'id' => 'interest_rate']) !!}
                </div>
                <div class="col-sm-4">
                  {!! Form::select('terms',  ['' => 'Terms', '2' => '2 months', '3' => '3 months', '6' => '6 months', '9' => '9 months', '12' => '12 months'], null, ['id' => 'terms', 'class' => 'form-control', 'required']) !!}
                </div>
              </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
          {!! Form::close() !!}

        </div>
      </div>
    </div>
  </div>

  <br>

  <div class="col-sm-12 col-sm-offset-2">
    <table id="clients-data-table" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Full Name</th>
                <th>Birthdate</th>
                <th>Gender</th>
                <th>Email</th>
                <th>Landline</th>
                <th>Cellphone</th>
                <th>Address</th>
                <th>Applied Loan Amount</th>
                <th>Loan Type</th>
                <th>Interest Rate</th>
                <th>Terms</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($clients as $client)
            <tr>
                <td>{{ $client->first_name }} {{ $client->middle_name }} {{ $client->last_name }}</td>
                <td>{{ $client->birthdate }}</td>
                <td>{{ $client->gender }}</td>
                <td>{{ $client->email }}</td>
                <td>{{ $client->landline }}</td>
                <td>{{ $client->cellphone }}</td>
                <td>{{ $client->address }}</td>
                <td>{{ $client->applied_loan_amount }}</td>
                <td>{{ $client->loan_type }}</td>
                <td>{{ $client->interest_rate }}</td>
                <td>{{ $client->terms }} months</td>
                <td>
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal">
                    Edit
                  </button>
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#deleteModal">
                    Delete
                  </button>
                </td>
            </tr>
                <!-- Modal -->
                <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="editModalLabel">Edit Client</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>

                      {!! Form::open([ 'method' => 'PATCH', 'class' => 'margin-t-5', 'route' => ['clients.update', $client->client_id], 'id' => 'editForm' ]) !!}
                      <div class="modal-body">
                          <div class="form-group row">
                            <div class="col-sm-4">
                              {!! Form::text('first_name', $client->first_name, ['class' => 'form-control', 'id' => 'edit_first_name', 'placeholder' => 'First Name']) !!}
                            </div>
                            <div class="col-sm-4">
                              {!! Form::text('middle_name', $client->middle_name, ['class' => 'form-control', 'id' => 'edit_middle_name', 'placeholder' => 'Middle Name']) !!}
                            </div>
                            <div class="col-sm-4">
                              {!! Form::text('last_name', $client->last_name, ['class' => 'form-control', 'id' => 'edit_last_name', 'placeholder' => 'Last Name']) !!}
                            </div>
                          </div>

                          <div class="form-group row">
                            <div class="col-sm-4">
                              {!! Form::email('email', $client->email, ['class' => 'form-control', 'id' => 'edit_email', 'placeholder' => 'Email']) !!}
                            </div>
                            <div class="col-sm-4">
                              {!! Form::text('landline', $client->landline, ['class' => 'form-control', 'id' => 'edit_landline', 'placeholder' => 'Landline']) !!}
                            </div>
                            <div class="col-sm-4">
                              {!! Form::text('cellphone', $client->cellphone, ['class' => 'form-control', 'id' => 'edit_cellphone', 'placeholder' => 'Cellphone']) !!}
                            </div>
                          </div>

                          <div class="form-group row">
                            <label for="birthdate" class="col-sm-4 col-form-label" style="text-align:right;">Birth date</label>
                            <div class="col-sm-4">
                              {!! Form::date('birthdate', Carbon\Carbon::parse($client->birthdate)->format('Y-m-d'), ['class' => 'form-control', 'id' => 'edit_birthdate']) !!}
                            </div>
                            <div class="col-sm-4">
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input class="form-check-input" type="radio" name="gender" id="male" value="male" {{ $client->gender == 'Male' ? 'checked' : null }}>
                                  Male
                                </label>
                              </div>
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input class="form-check-input" type="radio" name="gender" id="female" value="female" {{ $client->gender == 'Female' ? 'checked' : null }}>
                                  Female
                                </label>
                              </div>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label for="address" class="col-sm-4 col-form-label" style="text-align:right;">Address</label>
                            <div class="col-sm-8">
                              {!! Form::text('address', $client->address, ['class' => 'form-control', 'id' => 'edit_address']) !!}
                            </div>
                          </div>

                          <hr>

                          <div class="form-group row">
                            <label for="applied_loan_amount" class="col-sm-4 col-form-label" style="text-align:right;">Applied Loan Amount</label>
                            <div class="col-sm-4">
                              {!! Form::text('applied_loan_amount', $client->applied_loan_amount, ['class' => 'form-control', 'id' => 'edit_applied_loan_amount']) !!}
                            </div>
                            <div class="col-sm-4">
                              {!! Form::select('loan_type',  ['' => 'Loan Type', 'lite' => 'Lite Loan', 'salary' => 'Salary Loan', 'personal' => 'Personal Loan', 'business' => 'Business loan'], $client->loan_type, ['id' => 'edit_loan_type', 'class' => 'form-control', 'required']) !!}
                            </div>
                          </div>

                          <div class="form-group row">
                            <label for="interest_rate" class="col-sm-4 col-form-label" style="text-align:right;">Interest Rate</label>
                            <div class="col-sm-4">
                              {!! Form::text('interest_rate', $client->interest_rate, ['class' => 'form-control', 'id' => 'edit_interest_rate']) !!}
                            </div>
                            <div class="col-sm-4">
                              {!! Form::select('terms',  ['' => 'Terms', '2' => '2 months', '3' => '3 months', '6' => '6 months', '9' => '9 months', '12' => '12 months'], $client->terms, ['id' => 'edit_terms', 'class' => 'form-control', 'required']) !!}
                            </div>
                          </div>

                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                      </div>
                      {!! Form::close() !!}

                    </div>
                  </div>
                </div>
              </div>

              <!-- Modal -->
              <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="deleteModalLabel">Edit Client</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>

                    {!! Form::open([ 'method' => 'DELETE', 'class' => 'margin-t-5', 'route' => ['clients.destroy', $client->client_id], 'id' => 'deleteForm' ]) !!}
                    <div class="modal-body">
                        <div class="form-group row">
                          <div class="col-sm-12">
                            <label>Are you sure you want to delete this?</label>
                          </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    {!! Form::close() !!}

                  </div>
                </div>
              </div>
            </div>

            @endforeach
        </tbody>
    </table>
  </div>

<script>
    $(document).ready(function() {
        $('#clients-data-table').DataTable();
        $('.datepicker').datepicker();
    } );
</script>

</body>

</html>

