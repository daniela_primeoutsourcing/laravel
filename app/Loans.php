<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Loans extends Model
{
     use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'loans';
    protected $primaryKey = 'loan_id';
    protected $connection = 'mysql';
    protected $fillable = [
    	'client_id',
    	'account_number',
    	'applied_loan_amount',
    	'approved_loan_amount',
    	'amount_released',
    	'interest_rate',
    	'processing_fee_rate',
    	'processing_fee_amount',
    	'loan_type',
    	'terms',
    	'semi_monthly_amortization',
    	'monthly_amortization',
    	'pn',
    	'first_due_date',
    	'maturity_date',
    	'date_released'
    ];

    /*
     * Accessor and Mutator
     */

    public function setAppliedLoanAmountAttribute($applied_loan_amount) {
        $this->attributes['applied_loan_amount'] = str_replace(',', '', $applied_loan_amount);
    }

    public function setApprovedLoanAmountAttribute($approved_loan_amount) {
        $this->attributes['approved_loan_amount'] = str_replace(',', '', $approved_loan_amount);
    }

    public function setAmountReleasedAttribute($amount_released) {
        $this->attributes['amount_released'] = str_replace(',', '', $amount_released);
    }

    public function setSemiMonthlyAmortizationAttribute($semi_monthly_amortization) {
        $this->attributes['semi_monthly_amortization'] = str_replace(',', '', $semi_monthly_amortization);
    }

    public function setMonthlyAmortizationAttribute($monthly_amortization) {
        $this->attributes['monthly_amortization'] = str_replace(',', '', $monthly_amortization);
    }

    public function getAppliedLoanAmountAttribute($applied_loan_amount) {
        return number_format($applied_loan_amount, 2);
    }

    public function getApprovedLoanAmountAttribute($approved_loan_amount) {
        return number_format($approved_loan_amount, 2);
    }

    public function getAmountReleasedAttribute($amount_released) {
        return number_format($amount_released, 2);
    }

    public function getSemiMonthlyAmortizationAttribute($semi_monthly_amortization) {
        return number_format($semi_monthly_amortization, 2);
    }

    public function getMonthlyAmortizationAttribute($monthly_amortization) {
        return number_format($monthly_amortization, 2);
    }

    public function getPNAttribute($pn) {
        return number_format($pn, 2);
    }

    public function getFirstDueDateAttribute($first_due_date) {
        if ($first_due_date == '0000-00-00' || $first_due_date == null) {
            return null;
        } else {
            return Carbon::parse($first_due_date)->format('Y-m-d');
        }
    }

    public function getMaturityDateAttribute($maturity_date) {
        if ($maturity_date == '0000-00-00' || $maturity_date == null) {
            return null;
        } else {
            return Carbon::parse($maturity_date)->format('Y-m-d');
        }
    }

    public function getDateReleasedAttribute($date_released) {
        if ($date_released == '0000-00-00' || $date_released == null) {
            return null;
        } else {
            return Carbon::parse($date_released)->format('Y-m-d');
        }
    }
        
}
