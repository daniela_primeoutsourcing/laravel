<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Clients extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'clients';
    protected $primaryKey = 'client_id';
    /** 
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
    	'current_loan_id',
    	'first_name',
    	'middle_name',
    	'last_name',
    	'birthdate',
    	'gender',
    	'email',
    	'landline',
    	'cellphone',
    	'age',
    	'address',
    ];

    /*
     * Accessor and Mutator
     */

    public function setFirstNameAttribute($first_name) {
        $this->attributes['first_name'] = strtolower($first_name);
    }

    public function setMiddleNameAttribute($middle_name) {
        $this->attributes['middle_name'] = strtolower($middle_name);
    }

    public function setLastNameAttribute($last_name) {
        $this->attributes['last_name'] = strtolower($last_name);
    }

    public function setBirthdateAttribute($birthdate) {
        if ($birthdate != '') {
            $date = Carbon::parse($birthdate)->format('Y-m-d');
        } else {
            $date = null;
        }

        $this->attributes['birthdate'] = $date;
    }

    public function setGenderAttribute($gender) {
        $this->attributes['gender'] = strtolower($gender);
    }

    public function getFirstNameAttribute($first_name) {

        if (!empty($first_name)) {
            return ucwords($first_name);
        } else {
            return null;
        }
    }

    public function getMiddleNameAttribute($middle_name) {

        if (!empty($middle_name)) {
            return ucwords($middle_name);
        } else {
            return null;
        }
    }

    public function getLastNameAttribute($last_name) {
        if (!empty($last_name)) {
            return ucwords($last_name);
        } else {
            return null;
        }
    }

    public function getBirthdateAttribute($birthdate) {
        if ($birthdate == '0000-00-00') {
            return null;
        } else {
            return Carbon::parse($birthdate)->format('m/d/Y');
        }
    }

    public function getGenderAttribute($gender) {
        return ucfirst($gender);
    }

    public function getEmailAttribute($email) {
        if (!empty($email)) {
            return $email;
        } else {
            return null;
        }
    }

    public function getLandlineAttribute($landline) {
        if (!empty($landline)) {
            return $landline;
        } else {
            return null;
        }
    }

    public function getCellphoneAttribute($cellphone) {
        if (!empty($cellphone)) {
            return $cellphone;
        } else {
            return null;
        }
    }



}
